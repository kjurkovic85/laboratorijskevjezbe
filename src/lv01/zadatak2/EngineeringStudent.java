package lv01.zadatak2;

public class EngineeringStudent implements Human, Student {

    @Override
    public void watchTv() {
        System.out.println("Engineering student is rarely watching TV");
    }

    @Override
    public void sleep() {
        System.out.println("Engineering student is rarely sleeping");
    }

    @Override
    public void printYearAndCollegeName() {
        StringBuilder sb = new StringBuilder();
        sb.append("Year: ");
        sb.append(YEAR);
        sb.append("\n");
        sb.append("College name: ");
        sb.append(COLLEGE_NAME);
        System.out.println(sb.toString());
    }
}
