package lv01.zadatak2;

public interface Student {
    int YEAR = 4;
    String COLLEGE_NAME = "Tehnički fakultet";

    void printYearAndCollegeName();
}
