package lv01.zadatak2;

import lv01.Command;

public class Zadatak2 implements Command {

    /**
     * Cini mi se da je zadatak malo krivo definiran jer trazi da se pozivaju metode
     * eat(), sleep() i printYearAndCollegeName() dok oba interface-a imaju metode
     * sleep(), watchTv() i printYearAndCollegeName()
     *
     * U drugom zadatku koristimo Polimorfizam  kao koncept OOP-a
     * Polimorfizam jer EngineeringStudent i BadStudent možemo spremiti u varijablu tipa Student ili Human nad
     * kojima pozivamo metode na objektima EngineeringStudent i BadStudent
     */
    @Override
    public void execute() {
        Human firstHuman = new EngineeringStudent();
        Human secondHuman = new BadStudent();
        firstHuman.sleep();
        firstHuman.watchTv();
        secondHuman.sleep();
        secondHuman.watchTv();

        Student firstStudent = new EngineeringStudent();
        Student secondStudent = new BadStudent();
        firstStudent.printYearAndCollegeName();
        secondStudent.printYearAndCollegeName();
    }
}
