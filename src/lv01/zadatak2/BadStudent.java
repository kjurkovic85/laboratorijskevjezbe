package lv01.zadatak2;

public class BadStudent implements Human, Student {

    @Override
    public void watchTv() {
        System.out.println("Bad student is always watching TV");
    }

    @Override
    public void sleep() {
        System.out.println("Bad student is always sleeping");
    }

    @Override
    public void printYearAndCollegeName() {
        StringBuilder sb = new StringBuilder();
        sb.append("Year: ");
        sb.append(YEAR);
        sb.append("\n");
        sb.append("College name: ");
        sb.append(COLLEGE_NAME);
        System.out.println(sb.toString());
    }
}
