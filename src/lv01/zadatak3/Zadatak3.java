package lv01.zadatak3;

import lv01.Command;

import java.math.BigDecimal;

/**
 * U trecem zadatku koristimo Enkapsulaciju kao koncept OOP-a
 * Klasa Student sadrži privatne atribute koji su sakriveni korisnicima klasa. Korisnicima je dopušteno
 * pozivanje metoda koje koriste interne atribute i eventalno metode (ne u ovom slučaju)
 */
public class Zadatak3 implements Command {

    @Override
    public void execute() {
        Student student = new Student();
        student.setName("Kristijan Jurković");
        student.setYearOfStudy(3);
        student.setAverageGrade(BigDecimal.valueOf(3.5));

        StringBuilder sb = new StringBuilder();
        sb.append("Student name: ");
        sb.append(student.getName());
        sb.append("\n");
        sb.append("Year of study: ");
        sb.append(student.getYearOfStudy());
        sb.append("\n");
        sb.append("Average grade: ");
        sb.append(student.getAverageGrade().toPlainString());

        System.out.println(sb.toString());
    }
}
