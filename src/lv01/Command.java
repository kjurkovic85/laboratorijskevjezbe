package lv01;

public interface Command {

    void execute();
}
