package lv01;

import lv01.zadatak1.Zadatak1;
import lv01.zadatak2.Zadatak2;
import lv01.zadatak3.Zadatak3;
import lv01.zadatak4.Zadatak4;
import lv01.zadatak5.Zadatak5;
import lv01.zadatak6.Zadatak6;

public class ZadatakFactory {

    public static Command create(Zadatak zadatak) {
        switch (zadatak) {
            case ZADATAK1:
                return new Zadatak1();
            case ZADATAK2:
                return new Zadatak2();
            case ZADATAK3:
                return new Zadatak3();
            case ZADATAK4:
                return new Zadatak4();
            case ZADATAK5:
                return new Zadatak5();
            case ZADATAK6:
                return new Zadatak6();
            default:
                throw new IllegalStateException("Nevazeci zadatak");
        }
    }
}
