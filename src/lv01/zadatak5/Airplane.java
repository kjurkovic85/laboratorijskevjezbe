package lv01.zadatak5;

public class Airplane {

    void fly() {
        System.out.println("Avion leti");
    }

    void fly(int height) {
        System.out.println("Avion leti na visini " + height + " metara");
    }

    void fly(int length, String planeName) {
        System.out.println("Najduži let avionom bio je " + length + " kilometara, a let je odvozio avion naziva " + planeName);
    }
}
