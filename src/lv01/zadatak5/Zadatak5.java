package lv01.zadatak5;

import lv01.Command;

public class Zadatak5 implements Command {

    /**
     *  U petom zadatku koristimo Polimorfizam kao koncept OOP-a
     *  Polimorfizam jer Airplane klasa jer koristimo method overloading te time imamo
     *  3 metode istog imena ali drugog potpisa (method signature) jer sve 3 metode imaju različiti broj argumenata
     *  koje primaju
     */
    @Override
    public void execute() {
        Airplane plane = new Airplane();
        plane.fly();
        plane.fly(5);
        plane.fly(10000, "Boeing 474");
    }
}
