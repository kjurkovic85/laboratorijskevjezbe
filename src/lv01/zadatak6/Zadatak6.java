package lv01.zadatak6;

import lv01.Command;

/**
 *  U šestom zadatku koristimo samo klase za instanciranje objekta. Možemo i ovo definirati kao
 *  naslijeđivanje jer svaka klasa u Javi naslijeđuje Object klasu.
 */
public class Zadatak6 implements Command {

    @Override
    public void execute() {
        Student student = new Student();
        student.learn();

        BadStudent badStudent = new BadStudent();
        badStudent.learn();
    }
}
