package lv01.zadatak4;

public class Car extends Vehicle {

    private int wheelNumber = 4;
    private int doorNumber = 5;

    public Car(String engineType, String color) {
        super(engineType, color);
    }

    public int getWheelNumber() {
        return wheelNumber;
    }

    public int getDoorNumber() {
        return doorNumber;
    }
}
