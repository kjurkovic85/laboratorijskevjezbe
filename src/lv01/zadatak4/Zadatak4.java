package lv01.zadatak4;

import lv01.Command;

public class Zadatak4 implements Command {

    /**
     * U četvrtom zadatku koristimo Naslijeđivanje i Enkapsulaciju kao koncept OOP-a
     * Klase Car i Vehicle sadrže privatne atribute koji su sakriveni korisnicima klasa. Korisnicima je dopušteno
     * pozivanje metoda koje koriste interne atribute. Naslijeđivanje jer Car naslijeđuje klasu Vehicle te se kroz
     * objekt Car koriste metode klase Vehicle (printEngineType i printColor)
     */
    @Override
    public void execute() {
        Car car = new Car("diesel", "red");
        car.printColor();
        car.printEngineType();
        System.out.println("Door number: " + car.getDoorNumber());
        System.out.println("Wheel number" + car.getWheelNumber());
    }
}
