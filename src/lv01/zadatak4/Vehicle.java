package lv01.zadatak4;

public class Vehicle {
    private String engineType;
    private String color;

    public Vehicle(String engineType, String color) {
        this.engineType = engineType;
        this.color = color;
    }

    void printEngineType() {
        System.out.println("Engine type: " + engineType);
    }

    void printColor() {
        System.out.println("Color: " + color);
    }
}
