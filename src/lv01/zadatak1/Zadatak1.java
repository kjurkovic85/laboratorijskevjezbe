package lv01.zadatak1;

import lv01.Command;

public class Zadatak1 implements Command {

    /**
     * U prvom zadatku koristimo Naslijeđivanje i Polimorfizam kao koncept OOP-a
     * Naslijeđivanje jer klase BadStudent i GoodStudent naslijeđuju klasu Student
     * Polimorfizam jer GoodStudent i BadStudent možemo spremiti u varijablu tipa Student nad kojom pozivamo metode
     * na objektima GoodStudent i BadStudent
     */
    @Override
    public void execute() {
        Student dobarStudent = new GoodStudent();
        Student losStudent = new BadStudent();

        dobarStudent.intro();
        dobarStudent.learn();
        dobarStudent.sleep();

        losStudent.intro();
        losStudent.learn();
        losStudent.sleep();
    }



}
