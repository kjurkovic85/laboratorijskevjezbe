package lv01.zadatak1;

public class BadStudent extends Student {

    @Override
    void learn() {
        System.out.println("Loš student uči 1 sat na dan");
    }

    @Override
    void sleep() {
        System.out.println("Loš student spava 12 sati na dan");
    }
}
