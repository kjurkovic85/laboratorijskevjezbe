package lv01.zadatak1;

public class GoodStudent extends Student {

    @Override
    void learn() {
        System.out.println("Dobar student uči 10 sati na dan");
    }

    @Override
    void sleep() {
        System.out.println("Dobar student spava 8 sati na dan");
    }
}
