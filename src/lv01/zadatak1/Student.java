package lv01.zadatak1;

public abstract class Student {

    abstract void learn();
    abstract void sleep();

    void intro() {
        System.out.println("Studentski život");
    }
}
