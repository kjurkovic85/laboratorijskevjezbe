package lv01;

public enum Zadatak {
    ZADATAK1,
    ZADATAK2,
    ZADATAK3,
    ZADATAK4,
    ZADATAK5,
    ZADATAK6;

    static Zadatak forOrdinal(int value) {
        try {
            return values()[value];
        } catch (Exception e) {
            return null;
        }
    }
}
