package lv01;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        int unos = -1;
        while (unos != 0) {
            System.out.println("Unesite broj zadatka koji želite pokrenuti (1-6) ili 0 za izlaz: ");
            try (Scanner scanner = new Scanner(System.in)) {
                unos = scanner.nextInt();
                Zadatak zadatak = Zadatak.forOrdinal(unos - 1);
                Command command = ZadatakFactory.create(zadatak);
                command.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
